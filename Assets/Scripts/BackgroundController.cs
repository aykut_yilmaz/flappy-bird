﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour
{
    public List<SpriteRenderer> floors;

    private float floorWidth;
    private Transform rightSideFloor;

    void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        floors[0].transform.position = new Vector2(-GameConfigs.screenLeftBound,
            floors[0].transform.position.y);

        floors[1].transform.position = new Vector2(floors[0].bounds.max.x,
            floors[1].transform.position.y);

        rightSideFloor = floors[1].transform;

        floorWidth = floors[0].bounds.size.x * .99f;//to overlap sprites.
    }

    public void ManualUpdate(float deltaTime)
    {
        foreach (var item in floors)
        {
            item.transform.position += Vector3.left * deltaTime;

            //swap positions continiously to create a flow.
            if (item.transform.position.x + floorWidth < -GameConfigs.screenLeftBound)
            {
                item.transform.position = rightSideFloor.position +
                    new Vector3(floorWidth, 0, 0);

                rightSideFloor = item.transform;
            }
        }
    }
}
