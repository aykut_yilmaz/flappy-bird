﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum GameState
{
    InLobby,
    InGame,
    End
}

public class GameController : MonoBehaviour
{
    public PipeController pipeController;
    public BackgroundController backgroundController;
    public UIController uiController;
    public InputHandler inputHandler;
    public BirdScript birdScript;

    private GameState gameState;
    private int score;

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;

        pipeController.InitializeMap();
        birdScript.SetController(this);

        gameState = GameState.InLobby;

        inputHandler.onClickEvent += InputHandler_onClickEvent;
    }

    private void InputHandler_onClickEvent()
    {
        if (gameState == GameState.InLobby)
        {
            StartGame();
        }
        else if (gameState == GameState.InGame)
        {
            birdScript.Jump();
        }
        else if (gameState == GameState.End)
        {
            ResetGame();
        }
    }

    void Update()
    {
        if (gameState == GameState.InGame)
        {
            pipeController.ManualUpdate(Time.deltaTime * GameConfigs.gameSpeed);
            backgroundController.ManualUpdate(Time.deltaTime * GameConfigs.gameSpeed);
        }
    }

    private void StartGame()
    {
        gameState = GameState.InGame;
        birdScript.SetActive(true);
    }

    private void EndGame()
    {
        gameState = GameState.End;
    }

    private void ResetGame()
    {
        score = 0;
        uiController.SetScore(score);

        pipeController.ResetPipes();
        birdScript.ResetBird();
        gameState = GameState.InLobby;
    }

    public void BirdHitObstacle()
    {
        EndGame();
    }

    public void BirdHitScore()
    {
        if (gameState == GameState.InGame)
        {
            score++;
            uiController.SetScore(score);
        }
    }

}
