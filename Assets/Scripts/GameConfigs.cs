﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfigs
{
    public const float jumpForce = 400f;
    public const float pipeVerticalRangeMax = 2.5f;
    public const float pipeVerticalRangeMin = -1.0f;
    public const float distanceBetweenPipes = 3.5f;
    public const int initialPipeCount = 5;
    public const float screenLeftBound = 5.0f;
    public const float gameSpeed = 3.0f;

    GameConfigs()
    {
        
    }
}
