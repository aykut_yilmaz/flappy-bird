﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdScript : MonoBehaviour
{
    private Rigidbody2D rb;
    private GameController gameController;
    private Vector2 initialPosition;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        initialPosition = transform.position;
        SetActive(false);
    }

    public void SetController(GameController controller)
    {
        gameController = controller;
    }

    public void Jump()
    {
        rb.velocity = Vector2.zero;
        rb.AddForce(Vector2.up * GameConfigs.jumpForce);
    }

    public void SetActive(bool active)
    {
        rb.isKinematic = !active;
    }

    public void ResetBird()
    {
        transform.position = initialPosition;
        transform.rotation = Quaternion.identity;
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0f;
        SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "obstacle")
        {
            gameController.BirdHitObstacle();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        gameController.BirdHitScore();
    }
}
