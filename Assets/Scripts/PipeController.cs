﻿using System.Collections.Generic;
using UnityEngine;

public class PipeController : MonoBehaviour
{
    public GameObject pipePrefab;
    private GameObject lastPipe;

    private List<GameObject> pipes;

    void Awake()
    {
        pipes = new List<GameObject>();
    }

    public void InitializeMap()
    {
        for (int i = 0; i < GameConfigs.initialPipeCount; i++)
        {
            GameObject pipe = Instantiate(pipePrefab, transform);
            LocatePipe(pipe);
            pipes.Add(pipe);
        }
    }
    
    public void LocatePipe(GameObject pipe)
    {
        float posX = GameConfigs.distanceBetweenPipes;

        if (lastPipe != null)
        {
            posX += lastPipe.transform.position.x;
        }

        pipe.transform.position =
               new Vector2(posX,
                   Random.Range(GameConfigs.pipeVerticalRangeMin,
                   GameConfigs.pipeVerticalRangeMax));

        lastPipe = pipe;
    }

    public void ManualUpdate(float deltaTime)
    {
        foreach (var item in pipes)
        {
            item.transform.position += Vector3.left * deltaTime;

            if (item.transform.position.x < -GameConfigs.screenLeftBound)
            {
                LocatePipe(item);
            }
        }
    }

    //reposition all pipes.
    public void ResetPipes()
    {
        lastPipe = null;

        foreach (var item in pipes)
        {
            LocatePipe(item);
        }
    }
}
